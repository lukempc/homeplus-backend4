package com.homeplus.services;

import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.dtos.taskOffer.TaskOfferGetDto;
import com.homeplus.dtos.taskOffer.TaskOfferPostDto;
import com.homeplus.models.*;
import com.homeplus.repositories.TaskOfferRepository;
import com.homeplus.utility.mapper.TaskOfferMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskOfferService {

    private final TaskOfferRepository taskOfferRepository;
    private final TaskOfferMapper taskOfferMapper;
    private final TaskService taskService;
    private final TaskerService taskerService;
    private final UserService userService;

    public List<TaskOfferGetDto> getOffersByTaskId(Long id) {
        TaskEntity task = taskService.getTaskEntityById(id);
        return taskOfferRepository.findOffersByTask(task).stream()
                .map(offer -> taskOfferMapper.fromEntity(offer))
                .collect(Collectors.toList());
    }

    public Boolean checkIsOfferByTaskAndTasker(TaskEntity taskEntity, TaskerEntity taskerEntity) {
        return taskOfferRepository.findOfferByTaskAndTasker(taskEntity, taskerEntity).isPresent();
    }

    public void createTaskOffer(TaskOfferPostDto taskOfferPostDto) {

        TaskEntity task = taskService.getTaskEntityById(taskOfferPostDto.getTask_id());
        TaskerEntity tasker = taskerService.getTaskerEntityById(taskOfferPostDto.getTasker_id());
        if(task.getUserEntity() == tasker.getUserEntity()) {
            throw new IllegalStateException("You cannot apply for your own task");
        }

        Boolean isOffer = checkIsOfferByTaskAndTasker(task, tasker);
        if(isOffer) {
            throw new IllegalStateException("You already made an offer for this task");
        }
        taskOfferPostDto.setTaskEntity(task);
        taskOfferPostDto.setTaskerEntity(tasker);
        TaskOfferEntity taskOffer = taskOfferMapper.postDtoToEntity(taskOfferPostDto);

        taskOfferRepository.save(taskOffer);
    }

    public void updateOfferStatus(Long id, String reply_msg, TaskOfferStatus status) {
        taskOfferRepository.acceptOrRejectOffer(id, reply_msg, status, OffsetDateTime.now());

        if (status == TaskOfferStatus.ACCEPTED) {
            TaskOfferEntity offer = taskOfferRepository.findById(id).get();
            taskService.updateTaskerOfTask(offer.getTaskEntity(), offer.getTaskerEntity(), TaskStatus.assigned);
        }

        if (status == TaskOfferStatus.CANCELED) {
            TaskOfferEntity offer = taskOfferRepository.findById(id).get();
            taskService.updateTaskerOfTask(offer.getTaskEntity(), null, TaskStatus.open);
        }
    }

    public List<List<TaskOfferGetDto>> getPendingOffers(Long id) {
        List<TaskGetDto> user_tasks = taskService.getTasksByUidAndKeyword(id, "");

        return user_tasks.stream()
                .map(task -> toGetDto(taskOfferRepository.getPendingOffers(taskService.getTaskEntityById(task.getId()))))
                .collect(Collectors.toList());
    }

    public List<TaskOfferGetDto> getAcceptedOffers(Long id) {
        TaskerEntity taskerEntity = taskerService.getTaskerEntityById(id);
        return toGetDto(taskOfferRepository.getAcceptedOffers(taskerEntity));
    }

    public List<TaskOfferGetDto> toGetDto(Optional<TaskOfferEntity> taskOffers) {
        return taskOffers.stream()
                .map(taskOffer -> taskOfferMapper.fromEntity(taskOffer))
                .collect(Collectors.toList());
    }


    public TaskOfferGetDto getOffersById(Long id) {
        return taskOfferMapper.fromEntity(taskOfferRepository.findById(id).get());
    }
}
