package com.homeplus.services;

import com.homeplus.dtos.taskerReview.TaskReviewGetDto;
import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskerEntity;
import com.homeplus.repositories.TaskReviewRepository;
import com.homeplus.utility.mapper.TaskReviewMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskReviewService {

    private final TaskReviewRepository taskReviewRepository;
    private final TaskReviewMapper taskReviewMapper;
    private final TaskerService taskerService;
    private final TaskService taskService;

    public List<TaskReviewGetDto> getReviewsByTasker(Long id) {
        TaskerEntity taskerEntity = taskerService.getTaskerEntityById(id);
        List<TaskEntity> tasks = taskService.getTasksByTasker(taskerEntity);
        return tasks.stream()
                .map(task -> taskReviewRepository.getReviewsByTask(task))
                .map(review -> taskReviewMapper.fromEntity(review))
                .collect(Collectors.toList());
    }
}
