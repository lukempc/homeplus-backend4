package com.homeplus.services;

import com.homeplus.config.FrontEndUrlConfig;
import com.homeplus.dtos.AccountDto;
import com.homeplus.dtos.user.UserGetDto;
import com.homeplus.dtos.user.UserPutDto;
import com.homeplus.email.EmailSender;
import com.homeplus.exceptions.UserNotFoundException;
import com.homeplus.models.UserEntity;
import com.homeplus.repositories.UserRepository;
import com.homeplus.utility.mapper.UserGetMapper;
import com.homeplus.utility.mapper.UserInfoMapper;
import com.homeplus.utility.mapper.UserMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final UserInfoMapper userInfoMapper;
    private final FrontEndUrlConfig frontEndUrlConfig;
    //    private final UserGetDto userGetDto;
    private final UserGetMapper userGetMapper;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final EmailSender emailSender;

    private static final long jwtExpireTime = 1000 * 60 * 60 * 24;


    public UserEntity getUserById(Long id) {
        return userRepository.findById(id).get();
    }

    public UserGetDto getUserDtoById(Long id) {
        return userInfoMapper.fromEntity(getUserById(id));
    }

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.secretKey}")
    private String jwtSecretKey;

    public boolean ifEmailExists(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

    public void register(AccountDto accountDto) {
        UserEntity userEntity = userMapper.mapInfoDtoToEntity(accountDto);
        String encodedPassword = bCryptPasswordEncoder.encode(accountDto.getPassword());
        String token = this.generateRegisterToken();
        userEntity.setPassword(encodedPassword);
        userEntity.setToken(token);
        log.info("userEntity: " + userEntity);
        userRepository.save(userEntity);
        log.info("Save user successfully!");

        String email = accountDto.getEmail();
        String link = this.generateEmailVerifyLink(token);
        emailSender.send(email, this.buildEmail(accountDto.getName(), link));
        log.info("Email send successfully!");
    }

    public String generateRegisterToken(){
        return UUID.randomUUID().toString();
    }

    public String generateEmailVerifyLink(String token) {
        String frontEndUrl = frontEndUrlConfig.getFrontEndUrl();
//        String verifyLink = frontEndUrl + "/verifylink/verify?code=" + token;
        String verifyLink = frontEndUrl + "/account-verify/" + token;

        log.info("verifyLink: {}", verifyLink);
        return verifyLink;
    }

    @Transactional
    public String confirmEmailToken(String token) {
        try {
            userRepository.updateByToken(token);
        }catch (Exception e) {
            log.error("Failed to update user status to VERIFIED");
            throw new IllegalStateException("failed to send email");
        }
        return "confirmed";
    }

    public boolean isPasswordCorrect(String email, String password) {
        UserEntity userEntity = userRepository.findUserEntityByEmail(email).get();
        String encodedPassword = userEntity.getPassword();

        return bCryptPasswordEncoder.matches(password, encodedPassword);
    }

    public String generateJws(String email, String secretKey) {
        String jwtToken = Jwts.builder()
                .setHeaderParam("typ", "JWT")
                .setHeaderParam("alg", "HS512")
                .setSubject("login")
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + jwtExpireTime))
                .claim("email", email)
                .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()))
                .compact();
        log.info("jwt token: " + jwtToken);
        return jwtToken;
    }

    public Claims parseJwt(String jwt, String secretKey) {
        Jws<Claims> claimsJws = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(secretKey.getBytes()))
                .build()
                .parseClaimsJws(jwt);
        Claims body = claimsJws.getBody();
        return body;
    }

    public boolean validateJwt(String jwt, String secretKey){
        Claims body = parseJwt(jwt, secretKey);
        String email = String.valueOf(body.get("email"));

        if(!email.equals("")){
            return true;
        }
        return false;
    }

    public boolean isJwtValid(String jwt, String secretKey) {
        Claims body = parseJwt(jwt, secretKey);
        Long exp = body.getExpiration().getTime();

        System.out.println(exp);
        System.out.println(System.currentTimeMillis());
        if (exp <= System.currentTimeMillis()) {
            return false;
        }
        return true;
    }

    public String getEmailFromJwtToken(String jwt, String secretKey) {
        Claims body = parseJwt(jwt, secretKey);
        String email = String.valueOf(body.get("email"));

        return email;
    }



    public String findVerifyStatus(String email) {
        return userRepository.findUserVerifiedStatusByEmail(email);
    }

    public boolean isUserExists(String email) {
        return userRepository.findByEmail(email).isPresent();
    }

//    public UserGetDto getJwtToken(AccountDto accountDto) {
//        userGetDto.setUserEntity(this.findUserByEmail(accountDto.getEmail()));
//        // TODO: SET JWTTOKEN
//        return userGetDto;
//    }

    private UserEntity findUserByEmail(String email) {
        boolean userExists = userRepository.findByEmail(email).isPresent();
        if(!userExists) {
            throw new IllegalStateException("User with this email not exists: " + email);
        }else{
            return userRepository.findByEmail(email).get();
        }
    }

    public UserGetDto findUserGetDtoByEmail(String email) {
        return userGetMapper.getUserGetDtoFromEntity(userRepository.findUserEntityByEmail(email).get());
    }
//    public void sendEmail(String toEmail,
//                          String subject,
//                          String body) {
//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setFrom("colinwang321@gmail.com");
//        message.setTo(toEmail);
//        message.setText(body);
//        message.setSubject(subject);
//
//        javaMailSender.send(message);
//
//        System.out.println("Mail send successfully!");
//    }

    @Transactional
    public String confirmToken(String token) {
        try {
            userRepository.updateByToken(token);
        }catch (Exception e) {
            log.error("Failed to update user status to VERIFIED");
            throw new IllegalStateException("failed to send email");
        }
        return "confirmed";
    }

    private String buildEmail(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </blockquote>\n  <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

    public UserEntity findUserById(Long userId) {
        return userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("Cannot find user with id: " + userId));
    }

    public void turnOnOffTasker(Long id) {
        UserEntity user = findUserById(id);
        if(!user.getIs_tasker()){
            userRepository.turnOnTasker(id);
        } else {
            userRepository.turnOffTasker(id);
        }

    }

    public void updateUser(UserPutDto userPutDto) {
        UserEntity user = findUserById(userPutDto.getId());
        userPutDto.setPassword(user.getPassword());

        userRepository.save(userInfoMapper.putDtoToEntity(userPutDto));
    }

    public void updateIsTaskerData(Long id) {
        userRepository.updateIsTaskerData(id);
    }
}
