package com.homeplus.utility.mapper;

import com.homeplus.dtos.taskerReview.TaskReviewGetDto;
import com.homeplus.dtos.taskerReview.TaskReviewPostDto;
import com.homeplus.models.TaskReviewEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TaskReviewMapper {

    TaskReviewEntity postDtoToEntity(TaskReviewPostDto taskReviewPostDto);

    TaskReviewGetDto fromEntity(TaskReviewEntity taskReviewEntity);
}
