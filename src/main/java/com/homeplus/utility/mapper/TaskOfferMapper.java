package com.homeplus.utility.mapper;

import com.homeplus.dtos.taskOffer.TaskOfferGetDto;
import com.homeplus.dtos.taskOffer.TaskOfferPostDto;
import com.homeplus.dtos.taskOffer.TaskOfferPutDto;
import com.homeplus.models.TaskOfferEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.Optional;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TaskOfferMapper {

    TaskOfferEntity postDtoToEntity(TaskOfferPostDto taskOfferPostDto);

    TaskOfferEntity putDtoToEntity(TaskOfferPutDto taskOfferPutDto);

    TaskOfferGetDto fromEntity(TaskOfferEntity taskOfferEntity);
}
