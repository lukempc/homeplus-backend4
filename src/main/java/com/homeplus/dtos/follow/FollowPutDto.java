package com.homeplus.dtos.follow;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FollowPutDto {

    private Long id;

    private UserEntity userEntity;

    private TaskEntity taskEntity;

    private Boolean following;

    private OffsetDateTime created_time;

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}
