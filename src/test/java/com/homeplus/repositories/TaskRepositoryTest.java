package com.homeplus.repositories;

import com.homeplus.HomeplusApplication;
import com.homeplus.models.*;
import com.homeplus.utils.Utility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.OffsetDateTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@SpringBootTest(classes = HomeplusApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class TaskRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TaskerRepository taskerRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private TimeSectionsRepository timeSectionsRepository;

    @Autowired
    private Utility utility;

    int min = 0;
    int max = 100000;
    Integer random_int1 = (int)Math.floor(Math.random()*(max-min+1)+min);
    Integer random_int2 = (int)Math.floor(Math.random()*(max-min+1)+min);
    Integer random_int6 = (int)Math.floor(Math.random()*(max-min+1)+min);
    @Test
    public void shouldReturnUserEntitiesAndTaskerEntities() {
        // Create new userEntities.
        UserEntity userEntity1 = utility.buildUserEntity("ting" + random_int1 + "@gmail.com","Davin1", "testingOKOK1","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p1",OffsetDateTime.now(),OffsetDateTime.now());
        UserEntity userEntity2 = utility.buildUserEntity("ting" + random_int2 + "@gmail.com","Davin2", "testingOKOK2","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p2",OffsetDateTime.now(),OffsetDateTime.now());
        UserEntity saveduserEntity1 = userRepository.save(userEntity1);
        UserEntity saveduserEntity2 = userRepository.save(userEntity2);

        // Create new taskerEntities.
        TaskerEntity taskerEntity1 = utility.buildTaskerEntity(saveduserEntity1, "q1","q","IT support", "testing", "063-963", "132621233",OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity2 = utility.buildTaskerEntity(saveduserEntity2, "q2","q","Developer Programmmer", "testing", "063-955", "132626666",OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity savedtaskerEntity = taskerRepository.save(taskerEntity1);
        taskerRepository.save(taskerEntity2);


        List<UserEntity> userEntityList = userRepository.findAll();
        List<TaskerEntity> taskerEntityList = taskerRepository.findAll();
        assertNotNull(userEntityList);
        assertNotNull(taskerEntityList);
        assertTrue(userEntityList.size() >= 2);
        assertTrue(taskerEntityList.size() >= 2);
    }

    @Test
    public void itShouldFindTaskEntitiesById() {
        UserEntity userEntity2 = utility.buildUserEntity("ting" + random_int6 + "@gmail.com","Davin2", "testingOKOK2","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p2",OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity2 = utility.buildTaskerEntity(userEntity2, "q2","q","Developer Programmmer", "testing", "063-955", "132626666",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        UserEntity saveduserEntity = userRepository.save(userEntity2);
        TaskerEntity savedtaskerEntity = taskerRepository.save(taskerEntity2);

        // Given
        // Create a new task.
        TaskEntity task1 = utility.buildTaskEntity(saveduserEntity, savedtaskerEntity, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1, 5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1,TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());

        // When
       Optional<TaskEntity> taskEntityByID = taskRepository.findById(task1.getId());

        // Then
        assertNotNull(taskEntityByID);
    }

    @Test
    public void itShouldFindByKeyword() {
        UserEntity userEntity2 = utility.buildUserEntity("ting" + random_int6 + "@gmail.com","Davin2", "testingOKOK2","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p2",OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity2 = utility.buildTaskerEntity(userEntity2, "q2","q","Developer Programmmer", "testing", "063-955", "132626666",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        UserEntity saveduserEntity = userRepository.save(userEntity2);
        TaskerEntity savedtaskerEntity = taskerRepository.save(taskerEntity2);

        // Given
        // Create a new task.
        TaskEntity task1 = utility.buildTaskEntity(saveduserEntity, savedtaskerEntity, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1, 5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1,TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());


        // When
        String keyword = "Clean my";
        List<TaskEntity> taskEntityByKeyword = taskRepository.findByKeyword(keyword);

        // Then
        assertNotNull(taskEntityByKeyword);
    }


    @Test
    void itShouldFindTasksByUserID() {
        UserEntity userEntity2 = utility.buildUserEntity("ting" + random_int6 + "@gmail.com","Davin2", "testingOKOK2","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p2",OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity2 = utility.buildTaskerEntity(userEntity2, "q2","q","Developer Programmmer", "testing", "063-955", "132626666",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        UserEntity saveduserEntity = userRepository.save(userEntity2);
        TaskerEntity savedtaskerEntity = taskerRepository.save(taskerEntity2);

        // Given
        // Create a new task.
        TaskEntity task1 = utility.buildTaskEntity(saveduserEntity, savedtaskerEntity, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1, 5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1,TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());


        // When
        List<TaskEntity> taskEntityByUserId = taskRepository.findByUserId(saveduserEntity);

        // Then
        assertNotNull(taskEntityByUserId);
    }

    @Test
    void itShouldFindByTaskerId() {
        UserEntity userEntity2 = utility.buildUserEntity("ting" + random_int6 + "@gmail.com","Davin2", "testingOKOK2","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p2",OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity2 = utility.buildTaskerEntity(userEntity2, "q2","q","Developer Programmmer", "testing", "063-955", "132626666",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        UserEntity saveduserEntity = userRepository.save(userEntity2);
        TaskerEntity savedtaskerEntity = taskerRepository.save(taskerEntity2);

        // Given
        // Create a new task.
        TaskEntity task1 = utility.buildTaskEntity(saveduserEntity, savedtaskerEntity, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1,5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1,TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());


        // When
        List<TaskEntity> taskEntityByTaskerId = taskRepository.findByTaskerId(savedtaskerEntity);

        // Then
        assertNotNull(taskEntityByTaskerId);
    }

    @Test
    void itShouldFindUpComingTasksByUid() {
        UserEntity userEntity2 = utility.buildUserEntity("ting" + random_int6 + "@gmail.com","Davin2", "testingOKOK2","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p2",OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity2 = utility.buildTaskerEntity(userEntity2, "q2","q","Developer Programmmer", "testing", "063-955", "132626666",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        UserEntity saveduserEntity = userRepository.save(userEntity2);
        TaskerEntity savedtaskerEntity = taskerRepository.save(taskerEntity2);

        // Given
        // Create a new task.
        TaskEntity task1 = utility.buildTaskEntity(saveduserEntity, savedtaskerEntity, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1, 5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1,TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());


        // When
        List<TaskEntity> taskEntityByUpComingTasksAndUid = taskRepository.findUpComingTasksByUid(saveduserEntity, OffsetDateTime.now(), OffsetDateTime.now().plusDays(3));

        // Then
        assertNotNull(taskEntityByUpComingTasksAndUid);
    }

    @Test
    void itShouldFindUpComingTasksByTid() {
        UserEntity userEntity2 = utility.buildUserEntity("ting" + random_int6 + "@gmail.com","Davin2", "testingOKOK2","M","En","placeholder","placeholder","placeholder",new Date(),04211252, true, "p", "p2",OffsetDateTime.now(),OffsetDateTime.now());
        TaskerEntity taskerEntity2 = utility.buildTaskerEntity(userEntity2, "q2","q","Developer Programmmer", "testing", "063-955", "132626666",OffsetDateTime.now(),OffsetDateTime.now());
        TimeSectionsEntity timeSectionsEntity1 = utility.buildTimeSectionsEntity("pnn", "vd");

        UserEntity saveduserEntity = userRepository.save(userEntity2);
        TaskerEntity savedtaskerEntity = taskerRepository.save(taskerEntity2);

        // Given
        // Create a new task.
        TaskEntity task1 = utility.buildTaskEntity(saveduserEntity, savedtaskerEntity, "Clean my house", "testing", OffsetDateTime.now(), timeSectionsEntity1, 5526L, "d","d","d",3022L,"d",7,7,9,true,true,"d","d","d","d",1,TaskStatus.open, OffsetDateTime.now(), OffsetDateTime.now());


        // When
        List<TaskEntity> taskEntityByUpComingTasksAndTid = taskRepository.findUpComingTasksByTid(savedtaskerEntity, OffsetDateTime.now(), OffsetDateTime.now().plusDays(3));

        // Then
        assertNotNull(taskEntityByUpComingTasksAndTid);
    }



}